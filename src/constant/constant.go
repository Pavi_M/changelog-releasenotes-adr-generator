package constant

const (
	Version                        = "1.0.0-SNAPSHOT"
	OutputDirectory                = "output"
	ReleaseNoteFileName            = "ReleaseNote-"
	ChangelogFileName              = "Changelog-"
	ChangelogDirectory             = "changelogs"
	ReleaseNoteDirectory           = "release-notes"
	ComponentsDescriptionDirectory = "components-description"
)
